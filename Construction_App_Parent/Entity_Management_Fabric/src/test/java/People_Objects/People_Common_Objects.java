package People_Objects;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import Utils.BaseDriver;
import Utils.WebDriverCommonLib;

public class People_Common_Objects extends BaseDriver{

	public People_Common_Objects(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	WebDriverCommonLib wdc=new WebDriverCommonLib(driver);
	Actions actions=new Actions(driver);
	WebDriverWait wait = new WebDriverWait(driver, 2);
	
	By DataIsBeingLoaded = By.xpath("//div[@id='Production Management']/preceding-sibling::div[contains(text(),'DATA IS BEING LOADED')]");
    @FindBy(xpath="//*[@svgicon='LeftPanePlus']")	public  WebElement clickPlusButton;
    @FindBy(xpath="//*[@class='header-popup']//*[@id='save']/div") public  WebElement headerpopupsave;
	@FindBy(xpath="//*[@class='header-popup']") public  WebElement headerpopup;
	By SearchIcon = By.cssSelector("mat-icon[mattooltip='SEARCH']");
	By FilterTextBox = By.cssSelector("input[id='filter']");
	By clickOnDeleteButton = By.xpath("//span[text()='Delete']");


   
	LocalDate localDate = LocalDate.now();
	String s1 = DateTimeFormatter.ofPattern("yyy/MM/dd").format(localDate);
	String s2 = DateTimeFormatter.ofPattern("dd").format(localDate);

	
	public void ClickPlusButton() {
		try {
			if(driver.findElement(DataIsBeingLoaded).getText().equalsIgnoreCase("DATA IS BEING LOADED")) {			
				WebDriverWait wait = new WebDriverWait(driver, 80);
				wait.until(ExpectedConditions.invisibilityOf(driver.findElement(DataIsBeingLoaded)));
			}
		} catch (Exception e) {
			System.out.println("Data is being loaded is not displayed");
		}
		wdc.waitForPageLoad();
		actions.click(driver.findElement(By.xpath("//*[@svgicon='LeftPanePlus']"))).build().perform();
		System.out.println("clicked on + button");

		try {
			if(headerpopup.isDisplayed()) {
				wdc.waitForExpactedElement(headerpopupsave);
				headerpopupsave.click();
			}
		}
		catch(Exception e) {
			System.out.println("headerpopup not displaying");
		}
	}
	
	public  void ClickEntityType() throws Exception {
    WebElement EntityType=driver.findElement(By.xpath("//*[@role='listbox']//*[contains(text(), 'Entity Type')]"));
    wdc.waitForExpactedElement(EntityType);
    actions.click(EntityType).build().perform();		
    System.out.println("clicked on Entity type");
     }
    public void selectCompanyEntityType() {
    wdc.waitForPageLoad();
    WebElement EntityType = driver.findElement(By.xpath("//*[@role='option']//*[contains(text(), 'Company')]"));
    wdc.waitForExpactedElement(EntityType);
    actions.click(EntityType).build().perform();
	wdc.waitForPageLoad();
    }

    public void enterCompanyName(String s) throws Exception{
    wdc.waitForPageLoad();
    WebElement companyname = driver.findElement(By.xpath("//*[@placeholder='Company Name']"));
    wdc.waitForExpactedElement(companyname);
    actions.doubleClick(companyname).sendKeys(s1 + " " +s).sendKeys(Keys.ENTER).build().perform();
    }
    public void enterWebsite(String s) throws Exception{
    wdc.waitForPageLoad();
    WebElement Website = driver.findElement(By.xpath("//*[@placeholder='Website']"));
    wdc.waitForExpactedElement(Website);
    actions.doubleClick(Website).sendKeys(s).sendKeys(Keys.ENTER).build().perform();
    }
    
    public void enterNo_of_Employess(String s) throws Exception{
    wdc.waitForPageLoad();    	
    WebElement NoOfEmployess = driver.findElement(By.xpath("//*[@placeholder='No of Employess']"));
    wdc.waitForExpactedElement(NoOfEmployess);
    actions.doubleClick(NoOfEmployess).sendKeys(s).sendKeys(Keys.ENTER).build().perform();
    }
    

     public void SaveButton() throws Exception{
     Thread.sleep(5000);
     wdc.waitForPageLoad();
     WebElement saveButton=driver.findElement(By.xpath("//*[@id='save']/div"));
     wdc.waitForExpactedElement(saveButton);
     actions.click(saveButton).build().perform();
     Thread.sleep(5000);
    }
     
	public void searchAndRightclick(String s) throws Exception {
		
		Thread.sleep(9000);
		System.out.println("searchicon");
		actions.click(driver.findElement(SearchIcon)).perform();
		
		
		actions.click(driver.findElement(FilterTextBox)).sendKeys(s).sendKeys(Keys.ENTER).perform();
		Thread.sleep(9000);


		wdc.imlicitlywaitfor_10();	
		List<WebElement> nodetext=driver.findElements(By.xpath("//*[@id ='tree-areas']//*[contains(@class,'nodeText title')]"));	        	

		for(WebElement e:nodetext) {
			Thread.sleep(5000);
			System.out.println("location===="+e.getText());
			if(e.getText().equals(s)) {			
				actions.contextClick(e).build().perform();
				System.out.println("Context Clicked On Location ="+s);
				Thread.sleep(10000);
				break;
			}
		}
	}
	
    public void updateCompanyName(String s) throws Exception{
    wdc.waitForPageLoad();
    WebElement updateCompanyname = driver.findElement(By.xpath("//*[@placeholder='CompanyName']"));
    wdc.waitForExpactedElement(updateCompanyname);
    actions.doubleClick(updateCompanyname).sendKeys(s).sendKeys(Keys.ENTER).build().perform();
    }
	
	public void CompanyDelete() throws Exception {
	wdc.waitForPageLoad();
	wdc.waitForExpactedElement_150(driver.findElement(clickOnDeleteButton));
	actions.click(driver.findElement(clickOnDeleteButton)).build().perform();
	wdc.waitForPageLoad();		
	}
	
    public void selectPersonEntityType() {
    wdc.waitForPageLoad();
    WebElement EntityType = driver.findElement(By.xpath("//*[@role='option']//*[contains(text(), 'Person')]"));
    wdc.waitForExpactedElement(EntityType);
    actions.click(EntityType).build().perform();
	wdc.waitForPageLoad();
    }
	public void firstname(String s) {
		WebElement firstname = driver.findElement(By.xpath("//*[contains(@class,'input')]//*[@placeholder='First Name']"));
		wdc.waitForExpactedElement(firstname);
		actions.click(firstname)
		.sendKeys(s1 +" "+s)
		.build()
		.perform();
	}
	public void lastname(String s) {
		WebElement lastname = driver.findElement(By.xpath("//*[contains(@class,'input')]//*[@placeholder='Last Name']"));
		wdc.waitForExpactedElement(lastname);
		actions.click(lastname)
		.sendKeys(s)
		.build()
		.perform();
	}
	
	public void Company(String s) {
		WebElement company = driver.findElement(By.xpath("//div[@class='ng-placeholder'][contains(text(),'Company')]"));
		wdc.waitForExpactedElement(company);
		actions.click(company).sendKeys(s).perform();
		List<WebElement> list=driver.findElements(By.xpath("//*[@role='option']"));
		int i=0;
		for(WebElement e:list) {
			wdc.waitForPageLoad();
			if(e.getText().equals(s)) {
				System.out.println(e.getText());
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].scrollIntoView();", list.get(i));	
				actions.click(list.get(i)).build().perform();			
				break;
			}
			i++;			
		}
	}
	public void Email(String s) {
	    wdc.waitForPageLoad();
		WebElement email = driver.findElement(By.xpath("//*[contains(@class,'input')]//*[@placeholder='Email']"));
		wdc.waitForExpactedElement(email);
	    actions.click(email).sendKeys(s2+s).sendKeys(Keys.ENTER).build().perform();
	    wdc.waitForPageLoad();
	}
	public void Mobile(String s) {
		WebElement mobile = driver.findElement(By.xpath("//*[contains(@class,'input')]//*[@placeholder='Mobile']"));
		wdc.waitForExpactedElement(mobile);
		actions.click(mobile)
		.sendKeys(s)
		.build()
		.perform();
	}
	public void Time_Zone(String s) throws Exception {
		WebElement timezone = driver.findElement(By.xpath("//*[@role='listbox']//*[contains(text(), 'TimeZone')]"));
		wdc.waitForExpactedElement(timezone);
		actions.click(timezone);
		Thread.sleep(3000);
		actions.sendKeys(s)
		.sendKeys(Keys.ENTER)
		.build()
		.perform();
	}
	
	

}
