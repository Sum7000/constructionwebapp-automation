package People_Scripts;

import org.testng.annotations.Test;

import EntityManagement_POM.EntityManagement_Objects;
import People_Objects.People_Common_Objects;
import Utils.Constant;
import Utils.LoginSetup;

public class CompanyName_Creation extends LoginSetup{
	@Test
	public void company_Creation() throws Exception {
		System.out.println("Company Creation-----Excecution Started");
		EntityManagement_Objects entityManagement_Objects=new EntityManagement_Objects(driver);
		People_Common_Objects people_Common_Objects=new People_Common_Objects(driver);
		
		
		entityManagement_Objects.clickOnLeftTree_PeoplAndCompanies();
		entityManagement_Objects.ClickPlusButton_PeopleAndCompanies();
		people_Common_Objects.ClickEntityType();
		people_Common_Objects.selectCompanyEntityType();
		people_Common_Objects.enterCompanyName(Constant.CompanyName);
		people_Common_Objects.enterWebsite(Constant.Website);
		people_Common_Objects.enterNo_of_Employess(Constant.NoOfEmployess);
		people_Common_Objects.SaveButton();
		
		System.out.println("Company Creation-----Excecution Succesfully");

	}

}
