package People_Scripts;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import org.testng.annotations.Test;

import EntityManagement_POM.EntityManagement_Objects;
import People_Objects.People_Common_Objects;
import SearchEntities.Searchentitiesobjects;
import Utils.Constant;
import Utils.LoginSetup;
public class CompanyName_Updation extends LoginSetup{
	@Test
	public void company_Updation() throws Exception {
		
		System.out.println("CompanyName_Updation-----Excecution Started");
		Searchentitiesobjects Searchentitiesobjects = new Searchentitiesobjects(driver);
		People_Common_Objects people_Common_Objects=new People_Common_Objects(driver);
		EntityManagement_Objects entityManagement_Objects=new EntityManagement_Objects(driver);


		
		LocalDate localDate = LocalDate.now();
		String s1 = DateTimeFormatter.ofPattern("yyy/MM/dd").format(localDate);
		
		entityManagement_Objects.clickOnLeftTree_PeoplAndCompanies();
		Searchentitiesobjects.SearchEntity(s1+" "+Constant.CompanyName);
		people_Common_Objects.updateCompanyName(Constant.UpdateCompanyName);
		people_Common_Objects.enterWebsite(Constant.UpdateWebsite);
		people_Common_Objects.enterNo_of_Employess(Constant.UpdateNoOfEmployess);
		people_Common_Objects.SaveButton();
		
		System.out.println("CompanyName_Updation-----Excecution Succesfully");

		
	}

}
