package People_Scripts;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import org.testng.annotations.Test;

import EntityManagement_POM.EntityManagement_Objects;
import People_Objects.People_Common_Objects;
import Utils.Constant;
import Utils.LoginSetup;

public class CompanyName_Deletion extends LoginSetup{
	@Test
	public void company_Deletion() throws Exception {
		
		System.out.println("Company_Deletion-----Excecution Started");
		People_Common_Objects people_Common_Objects=new People_Common_Objects(driver);
		EntityManagement_Objects entityManagement_Objects=new EntityManagement_Objects(driver);

		
		LocalDate localDate = LocalDate.now();
		String s1 = DateTimeFormatter.ofPattern("yyy/MM/dd").format(localDate);
		
		entityManagement_Objects.clickOnLeftTree_PeoplAndCompanies();
		people_Common_Objects.searchAndRightclick(s1+" "+Constant.UpdateCompanyName);
		people_Common_Objects.CompanyDelete();
		System.out.println("Company_Deletion-----Excecution Sucessfull");

		
	}

}
