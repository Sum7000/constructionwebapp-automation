package Utils;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import Utils.LogoutSetup;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import io.github.bonigarcia.wdm.WebDriverManager;

public class LoginSetup{
	
	private final DriverManagerType CHROME = null;

	public WebDriver driver;

	public Properties prop = new Properties();
	
	public String browser;
	public FileInputStream fis;

	By username = By.xpath("//input[@name='username']");
	By nextbutton = By.xpath("//input[@value='NEXT']");
	By password = By.xpath("//input[@id='password']");
	By LOGIN = By.xpath("//input[@value='LOGIN']");
	By blinkingText=By.xpath("//*[text()='VERIFYING DEVICE.']");
	By menubutton=By.xpath("//*[@mattooltip='MENU']");
	By lefttreeConstrucion=By.xpath("//*[@src='wwwroot/images/fabricIcons/Construction_icon.svg']");
	By EntityManagementbutton=By.xpath("//*[@id='Entity Management']");
	By peopleAndCompanies=By.xpath("//*[@class='nav-bar']//*[1][@mattooltipposition='right']");

	public final String companyName = getCompanyName();
	public final String tenantName = getTenantName();
	public String userName = getUserName();
	public String urlname = getURLName();
	public String otpname = getOTP();
	public final String GET_URLIntegration = "https://qa-enterprises.visur.live:8797/GetOtp?key=" + userName + '@' + companyName+ '&' + otpname ;	
	public final String GET_URLQA      = "https://qa-enterprises.visur.tech:8797/GetOtp?key=" + userName + '@' + tenantName;
	public final String GET_URLPRod = "https://qa-enterprises.visur.io:8797/GetOtp?key=" + userName + '@' + tenantName;

	public String OTP;
	By errorMsg=By.xpath("//*[text()='Please enter the Verification Code.']");
	By errorMsg2=By.xpath("//*[text()='Please enter a valid Verification Code.']");
	
	By mainHeader=By.xpath("//*[@id='main-header']");
	By oneMomentPlease=By.xpath("//*[text()='One moment please ...']");

	By errormessage = By.xpath("//*[@class='error-message']");
	By otppage = By.xpath("//*[@id='otpCode']");
	


	public void sendGET() throws IOException {
		
		System.out.println(tenantName);
		
		URL url = null;
		if(urlname.contains("visur.live")) {
			System.out.println(GET_URLIntegration);
			url = new URL(GET_URLIntegration);
		} 
		else if(urlname.contains("visur.io")){
			System.out.println(GET_URLPRod);
			url = new URL(GET_URLPRod);
			
		}
		else if(urlname.contains("visur.tech")){
			System.out.println(GET_URLQA);
			url = new URL(GET_URLQA);
		}
		
		////////////////To handle ssl certificate//////////////
	
		TrustManager[] trustAllCerts = new TrustManager[]{
				new X509TrustManager() {
					public java.security.cert.X509Certificate[] getAcceptedIssuers() {
						return null;
					}
					public void checkClientTrusted(
							java.security.cert.X509Certificate[] certs, String authType) {
					}
					public void checkServerTrusted(
							java.security.cert.X509Certificate[] certs, String authType) {
					}
				}
		};

		// Install the all-trusting trust manager
		try {
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} 
		catch (Exception e) {
		}

		// Now you can access an https URL without having the certificate in the truststore

		HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
		httpURLConnection.setRequestMethod("GET");
		httpURLConnection.connect();

	System.out.println("code==="+httpURLConnection.getResponseCode() );
	     
		if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
			System.out.println("if");

			BufferedReader bufferedReaderInput = new BufferedReader(
					new InputStreamReader(httpURLConnection.getInputStream()));

			String inputLine;

			StringBuffer stringBufferResponse = new StringBuffer();
			while ((inputLine = bufferedReaderInput.readLine()) != null) {
				stringBufferResponse.append(inputLine);
			}
			bufferedReaderInput.close();

			try {
				JSONObject jsonObj = new JSONObject(stringBufferResponse.toString());
				OTP = jsonObj.getString("otp");
				System.out.println(OTP);
			} 
			catch (JSONException e) {
				e.printStackTrace();
			}
		} 
		else {
			System.out.println("GET request not worked");
		}
	}

	@BeforeMethod
	public void LaunchBrowser() throws Exception {

		try {
			getUserName();
			if (prop.getProperty("browser").equals("firefox")) {

				System.setProperty("webdriver.gecko.driver",
						System.getProperty("user.dir") + "\\driver\\geckodriver.exe");
				driver = new FirefoxDriver();

			} 
			else if (prop.getProperty("browser").equals("chrome")) {
				try{
					WebDriverManager.chromedriver().setup();
					System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\driver\\chromedriver.exe");
					//BY USING THIS LINE WE ARE ABLE TO GET THE LATEST VERSION OF CHROME DRIVER
					WebDriverManager.getInstance(CHROME).setup();
					//By using this line we'll able to clear the cache memory
					WebDriverManager.chromedriver().clearDriverCache().clearResolutionCache();
					//mention the below chrome option to solve timeout exception issue
					ChromeOptions options = new ChromeOptions();
					options.setPageLoadStrategy(PageLoadStrategy.NONE);
					//remove the timeout exception for taking screenshot
					options.addArguments("--disable-features=VizDisplayCompositor");
					//Run Chrome in incognito mode
					options.addArguments("--incognito");
					options.addArguments("disable-infobars","--verbose","--disable-web-security","--ignore-certificate-errors"
							,"--allow-running-insecure-content","--allow-insecure-localhost");
					
					LoggingPreferences logs = new LoggingPreferences();
					  logs.enable(LogType.PERFORMANCE, Level.ALL);
					  options.setCapability(CapabilityType.LOGGING_PREFS, logs);
					  options.setAcceptInsecureCerts(true);
					DesiredCapabilities capabilities = DesiredCapabilities.chrome();
					capabilities.setCapability(ChromeOptions.CAPABILITY, options);
					capabilities.setCapability("applicationCacheEnabled", false);
					// Instantiate the chrome driver
					driver = new ChromeDriver(options);
					//Clearing the cache memory of chrome.
					driver.get("chrome://settings/clearBrowserData");
					driver.switchTo().activeElement();
					driver.findElement(By.cssSelector("* /deep/ #clearBrowsingDataConfirm")).click();
					//driver = new ChromeDriver();
					System.setProperty("webdriver.chrome.silentOutput","true");
				}
				catch(Exception e){
				}
			} 
			else if (prop.getProperty("browser").equals("ie")) {

				System.setProperty("webdriver.ie.driver",
				System.getProperty("user.dir") + "\\driver\\IEDriverServer.exe");
				driver = new InternetExplorerDriver();
			}
			
		System.out.println(prop.getProperty("URL"));
		String url = prop.getProperty("URL");
		System.out.println(url);
		driver.get(url);
		driver.manage().window().maximize();
		
		LocalDate localDate = LocalDate.now();
		String s1 = DateTimeFormatter.ofPattern("0.yMMdd").format(localDate);

		System.out.println(s1);

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		try {
			if(driver.findElement(blinkingText).isDisplayed()) {
				System.out.println("Now verifying device is displaying");
				WebDriverWait wait = new WebDriverWait(driver, 20);
				wait.until(ExpectedConditions.invisibilityOf(driver.findElement(blinkingText)));
			}
		}
		catch (Exception e) {
			System.out.println("The verifying device is not displaying");
		}

		/*
		 * Here It'll Enter User Name & click on NEXT button.
		 */

		try {
			do {
				try {
					WebDriverWait wait = new WebDriverWait(driver, 60);
					if (driver.findElement(username).isDisplayed()) {
						System.out.println("User Name field displayed.");
						driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
						driver.findElement(username).sendKeys(prop.getProperty("username"));
						driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
						if (driver.findElement(nextbutton).isDisplayed()) {
							System.out.println("Next button displayed.");
							driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
							wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(nextbutton)));
							driver.findElement(nextbutton).click();//do loop for click next until password page displayed
							System.out.println("Clicked On Next button ID PAGE");
							Thread.sleep(2000);
							driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
							try {
								if ( driver.getCurrentUrl().contains("UserNameAuthentication") || 
										driver.findElement(username).isDisplayed()
										|| driver.findElement(By.xpath("//*[text()='Please enter a valid email address.']")).isDisplayed()) {
									driver.navigate().refresh();
									System.out.println("If= refreshed in Enter User Name!!!");
									Thread.sleep(5000);
								}
							} catch (Exception e) {
								driver.navigate().refresh();
								System.out.println("If-Catch= refreshed in Enter User Name!!!");
								Thread.sleep(5000);
							}
							
						}
					}
				} catch (Exception e) {
					driver.navigate().refresh();
					System.out.println("Catch= refreshed in Enter User Name!!!");
					Thread.sleep(5000);
				}
			} while( driver.getCurrentUrl().contains("UserNameAuthentication") || driver.findElement(username).isDisplayed());
		} catch (Exception e) {
			System.out.println("Enter User Name page not displayed.");
		}
		
		

		/*
		 * Here It'll Enter Password & click on Login button.
		 */
		try {
			do {
				try {
					if (driver.findElement(password).isDisplayed()) {
						System.out.println("Enter Password page displayed.");
						driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
						driver.findElement(password).sendKeys(prop.getProperty("Password"));
						driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
						if (driver.findElement(LOGIN).isDisplayed()) {
							System.out.println("Login Button displayed.");
							WebDriverWait wait=new WebDriverWait(driver, 20);
							wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(LOGIN)));
//							Actions actions = new Actions(driver);
//							actions.click(driver.findElement(LOGIN)).build().perform();
							driver.findElement(LOGIN).click();
							System.out.println("Login Button Clicked.");
							Thread.sleep(4000);
							try {
								driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
								if (driver.getCurrentUrl().contains("PasswordAuthentication") ||
										driver.findElement(By.xpath("//*[text()='Please enter the password.']")).isDisplayed()
										|| driver.findElement(By.xpath("//*[text()='Please enter a valid password.']")).isDisplayed()) {
									System.out.println("Login didnt happen, Refreshing...");
									driver.navigate().refresh();
									System.out.println("refreshed!!");
									Thread.sleep(5000);
								}
							} catch (Exception e) {
							}
						}
					}
				} catch (Exception e) {
					driver.navigate().refresh();
					Thread.sleep(5000);
				}
			} while( driver.getCurrentUrl().contains("PasswordAuthentication") || driver.findElement(password).isDisplayed());
		} catch (Exception e) {
		}
		
		/*
		 * Here It'll Check OTP page & OTP validation.
		 */
		try {
			sendGET();
			if (driver.findElement(otppage).isDisplayed()) {
				System.out.println("Otp page displayed.");
				driver.findElement(otppage).sendKeys(OTP);
				driver.findElement(By.xpath("//*[@class='rectangle']")).click();
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				Thread.sleep(6000);
				//try {
//						if (driver.findElement(errorMsg).isDisplayed() || driver.findElement(errorMsg2).isDisplayed()) {
//							Actions actions = new Actions(driver);
//							actions.click(driver.findElement(otppage))
//							.keyDown(Keys.CONTROL)
//							.sendKeys("a")
//							.keyUp(Keys.CONTROL)
//							.sendKeys(Keys.BACK_SPACE)
//							.sendKeys("OTPUsed")
//							.sendKeys(Keys.ENTER)
//							.sendKeys(Keys.TAB)
//							.build()
//							.perform();
//							driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//							Thread.sleep(2000);
//							driver.findElement(By.xpath("//*[@class='rectangle']")).click();
//							driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//							Thread.sleep(2000);

					try {
						if (driver.findElement(errorMsg).isDisplayed() ) {
							System.out.println("OTP Error Msg Displayed !!!");
							driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
							driver.findElement(By.xpath("//*[@value ='Resend verification code.']")).click();
							driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
							System.out.println("Enter Valid OTP after OTPUsed ");
							sendGET();
							driver.findElement(otppage).sendKeys(OTP);
							driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
							Thread.sleep(2000);
							driver.findElement(By.xpath("//*[@class='rectangle']")).click();
							driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
						}
						else if (driver.findElement(errorMsg2).isDisplayed()) {
							System.out.println("OTP Error Msg Displayed !!!");
							driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
							driver.findElement(By.xpath("//*[@value ='Resend verification code.']")).click();
							driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
							System.out.println("Enter Valid OTP after OTPUsed ");
							sendGET();
							driver.findElement(otppage).sendKeys(OTP);
							driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
							Thread.sleep(2000);
							driver.findElement(By.xpath("//*[@class='rectangle']")).click();
							driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
						}
					} 
					catch (Exception e) {
						System.out.println("Error Msg Not Displayed!");
					}
				}
				//} 
//				catch (Exception e) {
//					//System.out.println("Error Msg 1 Not Displayed!");
//				}
			//}
		}
		catch(Exception ex) {
			System.out.println("OTP page not displayed!");
			Thread.sleep(5000);
		}

		/*
		 * Here It'll click on Menu button then Construction Button.
		 */
		int j=0;
		boolean temp=true;
			do {
				try {
					if (j<2) {
						WebDriverWait wait = new WebDriverWait(driver, 60);
						if (driver.findElement(mainHeader).isDisplayed()) {
							System.out.println("Main header Displayed.");
							driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
							wait.until(ExpectedConditions.visibilityOf(driver.findElement(menubutton)));
							if (driver.findElement(menubutton).isDisplayed()) {
								System.out.println("Menu Button displayed.");
								driver.findElement(menubutton).click();
								System.out.println("Menu button clicked.");
								driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
								wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id='sub-header']//*[@id='header']"))));
								if (driver.findElement(By.xpath("//*[@id='sub-header']//*[@id='header']")).isDisplayed()) {

									driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
									wait.until(ExpectedConditions.visibilityOf(driver.findElement(EntityManagementbutton)));
									driver.findElement(EntityManagementbutton).click();
									System.out.println("Entity Management Button clicked.");
									driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
									Thread.sleep(3000);
									if (driver.getCurrentUrl().contains("Management")) {
										System.out.println("Entity Management form displayed.");
										temp=false;
									}
								}
							}
						}
					}
					else {
						System.out.println("Menu button not clicked!");
						temp=false;
					}
				} catch (Exception e) {
					j++;
					driver.navigate().refresh();
					Thread.sleep(5000);
					try {
						if(driver.findElement(oneMomentPlease).isDisplayed()) {
							System.out.println("App loading slowly...");
							WebDriverWait wait = new WebDriverWait(driver, 60);
							wait.until(ExpectedConditions.invisibilityOf(driver.findElement(oneMomentPlease)));
						}
					} catch (Exception e2) {
					}
				}
			} while (temp);
		
		
			/*
			 * Here It'll check People And Companies displayed or not.
			 */
			
			try {
				do {
					driver.findElement(peopleAndCompanies).click();
					System.out.println("People And Companies Clicked.");
					try {
						if (driver.findElement(By.xpath("//*[text()=' People & Companies ']")).isDisplayed()) {
							System.out.println("Successfully Logged In... ... ...");
							break;
						}
					} catch (Exception e) {
						driver.navigate().refresh();
						Thread.sleep(6000);
					}
				} while (driver.findElement(peopleAndCompanies).getAttribute("class").equalsIgnoreCase("headerIcon ng-star-inserted"));
			} catch (Exception e) {
				System.out.println("People And Companies Error!!! !!! !!! !!!");
			}

		}
		catch(Exception e) {
			System.out.println("login failed...???...???...???");
		}
	}

	@AfterMethod
	public void LogOut(ITestResult result) {
		if(ITestResult.FAILURE==result.getStatus()){
			try{
				// To create reference of TakesScreenshot
				TakesScreenshot screenshot=(TakesScreenshot)driver;
				// Call method to capture screenshot
				File src=screenshot.getScreenshotAs(OutputType.FILE);
				// Copy files to specific location 
				// result.getName() will return name of test case so that screenshot name will be same as test case name
				FileUtils.copyFile(src, new File(System.getProperty("user.dir")+ "\\ScreenShots\\"+result.getName()+".png"));
				System.out.println("Successfully captured a screenshot");
			}
			catch (Exception e){
				System.out.println("Exception while taking screenshot "+e.getMessage());
				try {
					driver.quit();
				} 
				catch (Exception ex) {
				}
			} 
		}

		try {
			LogoutSetup logoutSetup = new LogoutSetup(driver);
			logoutSetup.LogoutExcute();
			driver.quit();
		} catch (Exception e) {
			try {
				driver.quit();
			} catch (Exception ex) {
			}
		}
	}

	@AfterClass
	public void Quit() {
		try {
			driver.quit();
		} catch (Exception e) {
		}
	}
	
	public String getCompanyName() {
		try {
			fis = new FileInputStream(System.getProperty("user.dir") + "\\src\\main\\resources\\TestData.Properties");
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			prop.load(fis);
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		
		return prop.getProperty("CompanyName");
	}
	
	public String getOTP() {
		try {
			fis = new FileInputStream(System.getProperty("user.dir") + "\\src\\main\\resources\\TestData.Properties");
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			prop.load(fis);
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		
		return prop.getProperty("OTP");
	}

	public String getUserName() {
		try {
			fis = new FileInputStream(System.getProperty("user.dir") + "\\src\\main\\resources\\TestData.Properties");
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			prop.load(fis);
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		
		return prop.getProperty("username");
	}

	public String getURLName() {
		try {
			fis = new FileInputStream(System.getProperty("user.dir") + "\\src\\main\\resources\\TestData.Properties");
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			prop.load(fis);
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		
		return prop.getProperty("URL");
	}

	public String getTenantName() {
		try {
			fis = new FileInputStream(System.getProperty("user.dir") + "\\src\\main\\resources\\TestData.Properties");
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			prop.load(fis);
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		
		return prop.getProperty("TenantName");
	}
}