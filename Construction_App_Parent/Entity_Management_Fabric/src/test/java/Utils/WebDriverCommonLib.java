package Utils;

import java.io.FileInputStream;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WebDriverCommonLib extends LoginSetup{

	
	  public WebDriverCommonLib() { 
		  
		  super();
	  
	  }
	 
	public WebDriverCommonLib(WebDriver driver) {
	
		this.driver=driver;
		
	}


	public void waitForPageLoad() {
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
	}
	public void imlicitlywaitfor_10() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	public void imlicitlywaitfor_20() {
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}
	public void imlicitlywaitfor_30() {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	public void imlicitlywaitfor_40() {
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
	}
	public void imlicitlywaitfor_50() {
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
	}
	public void imlicitlywaitfor_60() {
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	}
	public void imlicitlywaitfor_80() {
		driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
	}
	public void imlicitlywaitfor_150() {
		driver.manage().timeouts().implicitlyWait(150, TimeUnit.SECONDS);
	}
	public void imlicitlywaitfor_1(WebElement element ) {
		WebDriverWait wait = new WebDriverWait(driver, 40);
		wait.until(ExpectedConditions.invisibilityOf(element));
	}
	

	/**
	 * This method is used to wait for a particular element need to load in GUI.
	 * @param WebElement
	 * @ExplicitlyWait.
	 */
	public void waitForExpactedElement(WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, 40);
		wait.until(ExpectedConditions.visibilityOf(element));
	}
	public void waitForExpactedElement_10(WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOf(element));
	}
	public void waitForExpactedElement_20(WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOf(element));
	}
	public void waitForExpactedElement_30(WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOf(element));
	}
	public void waitForExpactedElement_40(WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, 40);
		wait.until(ExpectedConditions.visibilityOf(element));
	}
	public void waitForExpactedElement_50(WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(element));
	}
	public void waitForExpactedElement_60(WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOf(element));
	}
	public void waitForExpactedElement_80(WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		wait.until(ExpectedConditions.visibilityOf(element));
	}
	public void waitForExpactedElement_100(WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, 100);
		wait.until(ExpectedConditions.visibilityOf(element));
	}
	public void waitForExpactedElement_150(WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, 150);
		wait.until(ExpectedConditions.visibilityOf(element));
	}
	
	
	/**
	 * This method is used to wait for list of web elements present in the web page. 
	 * @param selectdataentry
	 */
	public void waitForExpectedAllElements(List<WebElement> list) {
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.visibilityOfAllElements(list));
		}
	
	/**
	 * It will wait for the element until click-able.
	 * @param element
	 */
	public void waitForExpectedElementToBeClickable(WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	
	/**
	 * read the data from Properties File based on key 
	 * @param key
	 * @return value of the Key
	 * @throws Throwable
	 */
	public String getPropertiesFileData(String key) {
		FileInputStream fis;
		Properties pobj = new Properties();
		try {
			fis= new FileInputStream("./resources/TestData.properties");
			pobj.load(fis);
		}
		catch(Exception e) {
			System.out.println("File Not Found, Please give proper file path");
		}
		String value = pobj.getProperty(key);
		return value;
	}
	
	/**
	 * this method will wait until the element disappeared from the GUI.
	 * @param element
	 */
	public void invisibilityOfElement(WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.invisibilityOf(element));
	}
}
