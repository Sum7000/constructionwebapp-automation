package CRUD_Operation_POM;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utils.BaseDriver;
import Utils.WebDriverCommonLib;

public class CRUD_Operation_CommonObjects extends BaseDriver {

	public CRUD_Operation_CommonObjects(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	Actions actions = new Actions(driver);
	WebDriverWait wait = new WebDriverWait(driver, 60);
	WebDriverCommonLib wdc = new WebDriverCommonLib(driver);
	JavascriptExecutor je = (JavascriptExecutor) driver;
	
	
	By entityTypes=By.xpath("//*[text()='Entity Type']/parent::div//*[@role='combobox']");
	By ParentEntity=By.xpath("//*[text()='Parent Entity']/parent::div//*[@role='combobox']");
	By EntityName=By.xpath("//*[@placeholder='Entity Name']");
	By SaveButton=By.xpath("//*[@id='save']//*[contains(@class,'head-route')]");
	By closeButton=By.xpath("//*[@id='close']//*[contains(@class,'head-route')]");
	By mapForm=By.xpath("//*[@class='maps']//*[@id='editor']");
	By renamePopup=By.xpath("//*[@id='title']//*[contains(text(),'Rename' )]");
	By renameInput=By.xpath("//*[@placeholder='Rename']");
	By attentionClose=By.xpath("//*[@id='dragHandler']//*[contains(@class,'head-route')]");
	By attentionPopup=By.xpath("//*[@id='dragHandler']//*[@id='dragHandler']");
	
	
	
	public void selectEntityType_Map() {
		wdc.waitForPageLoad();
		driver.findElement(entityTypes).click();
		wdc.waitForPageLoad();
		List<WebElement> list=driver.findElements(By.xpath("//*[@role='option']//span"));
		wdc.waitForExpectedAllElements(list);
		for (WebElement e:list) {
			System.out.println(e.getText());
			if (e.getText().equalsIgnoreCase("Map")) {
				wdc.waitForPageLoad();
				e.click();
				wdc.waitForPageLoad();
				break;
			}
		}
	}
	
	public void selectEntityType_Category() {
		wdc.waitForPageLoad();
		driver.findElement(entityTypes).click();
		wdc.waitForPageLoad();
		List<WebElement> list=driver.findElements(By.xpath("//*[@role='option']//span"));
		wdc.waitForExpectedAllElements(list);
		for (WebElement e:list) {
			System.out.println(e.getText());
			if (e.getText().equalsIgnoreCase("Category")) {
				wdc.waitForPageLoad();
				e.click();
				wdc.waitForPageLoad();
				break;
			}
		}
	}
	
	public void selectParentEntity(String parentEntityName) {
		wdc.waitForPageLoad();
		driver.findElement(ParentEntity).click();
		wdc.waitForPageLoad();
		List<WebElement> list=driver.findElements(By.xpath("//*[@role='option']//span"));
		wdc.waitForExpectedAllElements(list);
		for (WebElement e:list) {
			System.out.println(e.getText());
			if (e.getText().equalsIgnoreCase(parentEntityName)) {
				wdc.waitForPageLoad();
				e.click();
				wdc.waitForPageLoad();
				break;
			}
			scrollDown_ToElement(e);
		}
	}
	
	public void enterEntityName(String s) {
		wdc.waitForPageLoad();
		driver.findElement(EntityName).sendKeys(s);
		wdc.waitForPageLoad();
	}
	
	public void renameEntityName(String s) {
		try {
			wdc.waitForPageLoad();
			if (driver.findElement(renamePopup).isDisplayed()) {
				System.out.println("Rename popup form displayed.");
				driver.findElement(renameInput).sendKeys(s);
				wdc.waitForPageLoad();
			}
		} catch (Exception e) {
			System.out.println("Rename popup form not displayed.");
		}
	}

	public void clickOnSave() {
		wdc.waitForPageLoad();
		try {
			if (driver.findElement(SaveButton).isDisplayed()) {
				System.out.println("Save button displayed");
				driver.findElement(SaveButton).click();
				wdc.waitForPageLoad();
			}
		} catch (Exception e) {
			System.out.println("Save not button displayed");
		}
	}
	
	public void closeAttentionPopup() {
		wdc.waitForPageLoad();
		try {
			if (driver.findElement(attentionPopup).isDisplayed()) {
				System.out.println("Attention Popup displayed");
				driver.findElement(attentionClose).click();
				wdc.waitForPageLoad();
			}
		} catch (Exception e) {
			System.out.println("Attention Popup not displayed");
		}
	}
	
	public void clickOnClose() {
		wdc.waitForPageLoad();
		try {
			if (driver.findElement(closeButton).isDisplayed()) {
				System.out.println("Close button displayed");
				driver.findElement(closeButton).click();
				wdc.waitForPageLoad();
			}
		} catch (Exception e) {
			System.out.println("Close not button displayed");
		}
	}
	
	public void verifyCreated_Map() {
		try {
			wdc.waitForPageLoad();
			if (driver.findElement(mapForm).isDisplayed()) {
				System.out.println("Map Form Displayed");
			}
		} catch (Exception e) {
			System.out.println("Map Form Not Displayed?");
		}
	}
	
	public void scrollDown_ToElement(WebElement element) {
		try {
			je.executeScript("arguments[0].scrollIntoView(true);",element);
		} catch (Exception e) {
		}
		
	}
}
