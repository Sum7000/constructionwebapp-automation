
package Utils;

public class Constant {

	public static final String parentEntity = "QA_Automation_Category";
	public static final String creationMap_EntityName = " Automation_MAP";
	public static final String UpdateMap_EntityName = " Automation_MAP_Updated";
	public static final String creationCategory_EntityName = " Automation_Category";
	public static final String UpdateCategory_EntityName = " Automation_Category_Updated";
}

