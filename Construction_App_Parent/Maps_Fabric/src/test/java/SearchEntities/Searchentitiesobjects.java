
package SearchEntities;

import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utils.BaseDriver;
import Utils.WebDriverCommonLib;

public class Searchentitiesobjects extends BaseDriver{

	public Searchentitiesobjects(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	LocalDate localDate = LocalDate.now();
	String s1 = DateTimeFormatter.ofPattern("yyy/MM/dd").format(localDate);

	Actions actions = new Actions(driver);
	WebDriverWait wait = new WebDriverWait(driver, 2);
	WebDriverCommonLib wdc = new WebDriverCommonLib(driver);
	
	public String production,month ;
	WebElement source = null;

	By SearchIcon = By.xpath("//*[@id='filter-search']//mat-icon[@svgicon='MainSearch']");
	By FilterTextBox = By.xpath("//*[@placeholder='   Filter']");
	By CLOSE = By.xpath("//*[@svgicon='V3 CloseCancel']");
	By mapDataIsBeingLoaded = By.xpath("//*[@id='All']/../*[normalize-space(text())='DATA IS BEING LOADED']");
	By FormName = By.xpath("//*[@id='fdc-data-entry']//label");
	By productiondate = By.xpath("//*[@class='location-header']");
	
	


	public  void CLOSE(){
		driver.findElement(CLOSE).click(); 
	}
	
	public  void Searchicon(){
		actions.click(driver.findElement(SearchIcon)).perform();
	}
	
	public  void filterinput(String s){
		wdc.waitForExpactedElement(driver.findElement(FilterTextBox));
		actions.click(driver.findElement(FilterTextBox)).sendKeys(s).sendKeys(Keys.ENTER).perform();
	}
	
	/**
	 * This method is used to search the map present inside left tree and click on it.
	 * @param string 
	 * @throws Exception
	 */
	public  void SearchEntity_Map(String s) throws Exception{
		boolean obj=true;
		int i=0;
		while (obj) {
			if (i<3) {
				try {
					System.out.println("search");
					waitForDataLoading();
					clickOnSearch();
					clickOnFilterBox(s);
					wdc.waitForPageLoad();	
					clickOnMap(s);
					wdc.imlicitlywaitfor_80();
					if (driver.findElement(By.xpath("//*[contains(@id, 'form-header')]")).isDisplayed() && driver.findElement(By.xpath("//*[@id='form-generation']")).isDisplayed()) {
						System.out.println("Project form displayed.");
						obj=false;
					}
				}
				catch (Exception e) {
					i++;
					driver.navigate().refresh();
					System.out.println("refreshed in Search method.");
					Thread.sleep(10000);	
				}
			}
			else {
				obj=false;
				System.out.println("Searching Failed Due Tree Not Loading !!! !!! !!! !!!");
			}
		}
	}	
	
	/**
	 * This method is used to search the map and verify that exact map is present or not in left tree. 
	 * @param s
	 * @throws Exception
	 */
	public void searchAndVerify_isMapPresentInLeftTree(String s) throws Exception{
		boolean obj=true;
		int i=0;
		while (obj) {
			if (i<3) {
				try {
					System.out.println("search");
					waitForDataLoading();
					clickOnSearch();
					clickOnFilterBox(s);
					wdc.waitForPageLoad();	
					System.out.println("Trying to check Map In Left Tree... ...");
					wdc.waitForPageLoad();
					List<WebElement> nodetext=driver.findElements(By.xpath("//*[@id='All']//*[contains(@class,'nodeText title')]"));	
					List<WebElement> nodeAllTree=driver.findElements(By.xpath("//*[@id='All']//*[@class='angular-tree-component']//tree-node"));
					String hightOfTree=driver.findElement(By.xpath("//*[@id='All']//*[contains(@style, 'height')]")).getCssValue("height")
							.replace(" ", "").replaceAll("([a-z])", "");
					System.out.println("Tree hight is after search : "+ hightOfTree);
					int hight=Integer.parseInt(hightOfTree);
					wdc.imlicitlywaitfor_80();
					while (nodetext.size()!=0 && hight<250) {
						for(WebElement e:nodetext) {
							System.out.println("Map==== "+e.getText());
							Thread.sleep(2000);
							if(e.getText().equals(s)) {			
								System.out.println(s+" =Map is avialable in left tree");
								actions.click(driver.findElement(CLOSE)).build().perform();
								wdc.imlicitlywaitfor_80();
								Thread.sleep(3000);
								obj=false;
								break;
							}
						}
						break;
					}
				}
				catch (Exception e) {
					i++;
					driver.navigate().refresh();
					System.out.println("refreshed in Search method.");
					Thread.sleep(10000);	
				}
			}
			else {
				obj=false;
				System.out.println("Searching Failed Due Tree Not Loading !!! !!! !!! !!!");
			}
		}
	}
	
	/**
	 * This method is used to do search and context click from the left tree.
	 * @param s
	 * @throws Exception
	 */
	public void searchAndContextClick(String s) throws Exception{
		boolean obj=true;
		int i=0;
		while (obj) {
			if (i<3) {
				try {
					System.out.println("search");
					waitForDataLoading();
					clickOnSearch();
					clickOnFilterBox(s);
					wdc.waitForPageLoad();	
					System.out.println("Trying to check Map In Left Tree... ...");
					wdc.waitForPageLoad();
					List<WebElement> nodetext=driver.findElements(By.xpath("//*[@id='All']//*[contains(@class,'nodeText title')]"));	
					List<WebElement> nodeAllTree=driver.findElements(By.xpath("//*[@id='All']//*[@class='angular-tree-component']//tree-node"));
					String hightOfTree=driver.findElement(By.xpath("//*[@id='All']//*[contains(@style, 'height')]")).getCssValue("height")
							.replace(" ", "").replaceAll("([a-z])", "");
					System.out.println("Tree hight is after search : "+ hightOfTree);
					int hight=Integer.parseInt(hightOfTree);
					wdc.imlicitlywaitfor_80();
					while (nodetext.size()!=0 && hight<250) {
						for(WebElement e:nodetext) {
							System.out.println("Map==== "+e.getText());
							Thread.sleep(2000);
							if(e.getText().equals(s)) {		
								actions.contextClick(e).build().perform();
								wdc.waitForPageLoad();
								System.out.println(s+" =Context clicked");
								obj=false;
								break;
							}
						}
						break;
					}
				}
				catch (Exception e) {
					i++;
					driver.navigate().refresh();
					System.out.println("refreshed in Search method.");
					Thread.sleep(10000);	
				}
			}
			else {
				obj=false;
				System.out.println("Searching Failed Due Tree Not Loading !!! !!! !!! !!!");
			}
		}
	}
	
	public void clickOnSearch() throws Exception {
		wdc.waitForExpactedElement(driver.findElement(SearchIcon));
		if (driver.findElement(SearchIcon).isDisplayed()) {
			System.out.println("Search icon displayed.");
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			actions.click(driver.findElement(SearchIcon)).build().perform();
			System.out.println("searchicon");
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
	}
	
	public void clickOnFilterBox(String s) throws Exception {
		wdc.waitForExpactedElement(driver.findElement(FilterTextBox));
		if (driver.findElement(FilterTextBox).isDisplayed()) {
			System.out.println("Filter Box Displayed.");
			wdc.waitForExpactedElement(driver.findElement(FilterTextBox));
			actions.click(driver.findElement(FilterTextBox)).sendKeys(s).sendKeys(Keys.ENTER).build().perform();
			wdc.waitForPageLoad();
			Thread.sleep(2000);
		}
	}
	
	public void clickOnMap(String s) throws Exception {
		System.out.println("Trying to click on a location... ...");
		wdc.waitForPageLoad();
		List<WebElement> nodetext=driver.findElements(By.xpath("//*[@id='All']//*[contains(@class,'nodeText title')]"));	
		List<WebElement> nodeAllTree=driver.findElements(By.xpath("//*[@id='All']//*[@class='angular-tree-component']//tree-node"));
		String hightOfTree=driver.findElement(By.xpath("//*[@id='All']//*[contains(@style, 'height')]")).getCssValue("height")
				.replace(" ", "").replaceAll("([a-z])", "");
		System.out.println("Tree hight is after search : "+ hightOfTree);
		int hight=Integer.parseInt(hightOfTree);
		wdc.imlicitlywaitfor_80();
		while (nodetext.size()!=0 && hight<250) {
			for(WebElement e:nodetext) {
				System.out.println("Map==== "+e.getText());
				Thread.sleep(2000);
				if(e.getText().equals(s)) {			
					actions.click(e).build().perform();
					System.out.println("Map clicked.");
					actions.click(driver.findElement(CLOSE)).build().perform();
					wdc.imlicitlywaitfor_80();
					Thread.sleep(3000);
					break;
				}
			}
			break;
		}
	}
	
	
	public void waitForDataLoading() {
		try {
			wdc.waitForPageLoad();
			if(driver.findElement(mapDataIsBeingLoaded).isDisplayed()) {			
				System.out.println("DataIsBeingLoaded is displaying");
				WebDriverWait wait = new WebDriverWait(driver, 60);
				wait.until(ExpectedConditions.invisibilityOf(driver.findElement(mapDataIsBeingLoaded)));
			}
		}
		catch(Exception e) {
			System.out.println("DATA IS BEING LOADED is not displaying");
		}
	}	
}