package mapCommonObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import CRUD_Operation_POM.CRUD_Operation_CommonObjects;
import Utils.BaseDriver;
import Utils.WebDriverCommonLib;

public class MapCommonPOM extends BaseDriver{

	public MapCommonPOM(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	Actions actions = new Actions(driver);
	WebDriverWait wait = new WebDriverWait(driver, 60);
	WebDriverCommonLib wdc = new WebDriverCommonLib(driver);
	CRUD_Operation_CommonObjects crud_Operation_CommonObjects=new CRUD_Operation_CommonObjects(driver);
	
	By DataIsBeingLoaded = By.xpath("//*[@id='All']/../*[normalize-space(text())='DATA IS BEING LOADED']");
	By createNewEntityPopup=By.xpath("//*[text()=' Create New Entity ']/ancestor::div[contains(@class,'popup-form')]");
	By renameButton=By.xpath("//*[@role='menuitem']//*[text()='Rename']/..");
	By deleteButton=By.xpath("//*[@role='menuitem']//*[text()='Delete']/..");
	@FindBy(xpath="//*[@svgicon='LeftPanePlus']")
	public  WebElement clickPlusButton;
	@FindBy(xpath="//*[@class='header-popup']") 
	public  WebElement headerpopup;
	@FindBy(xpath="//*[@class='header-popup']//*[@id='save']/div") 
	public  WebElement headerpopupsave;
	
	
	
	public void ClickPlusButton_Maps() {
		try {
			if(driver.findElement(DataIsBeingLoaded).getText().equalsIgnoreCase("DATA IS BEING LOADED")) {			
				wait.until(ExpectedConditions.invisibilityOf(driver.findElement(DataIsBeingLoaded)));
			}
		}
		catch(Exception e) {
		}
		try {
			wdc.waitForExpectedElementToBeClickable(clickPlusButton);
			clickPlusButton.click();
			if(headerpopup.isDisplayed()) {
				wdc.waitForExpactedElement(headerpopupsave);
				headerpopupsave.click();
			}
		}
		catch(Exception e) {
		}
	}
	
	public void verifyCreationEntityForm_isDisplay() {
		try {
			wdc.waitForPageLoad();
			if (driver.findElement(createNewEntityPopup).isDisplayed()) {
				System.out.println("System is displaying Create New Entity Popup Form");
			}
		} catch (Exception e) {
			System.out.println("System is not displaying Create New Entity Popup Form");
		}
	}
	
	public void clickOnRename_ContextClick() {
		try {
			wdc.waitForPageLoad();
			if (driver.findElement(By.xpath("//*[@id='menu-list']")).isDisplayed()) {
				System.out.println("Context Menu Displayed.");
				driver.findElement(renameButton).click();
				System.out.println("Clicked on Rename Option");
				wdc.waitForPageLoad();
			}
		} catch (Exception e) {
			System.out.println("Context Menu not Displayed.");
		}
	}
	
	public void clickOnDelete_ContextClick() {
		try {
			wdc.waitForPageLoad();
			if (driver.findElement(By.xpath("//*[@id='menu-list']")).isDisplayed()) {
				System.out.println("Context Menu Displayed.");
				driver.findElement(deleteButton).click();
				System.out.println("Clicked on Delete Option");
				wdc.waitForPageLoad();
			}
		} catch (Exception e) {
			System.out.println("Context Menu not Displayed.");
		}
	}
	
	public void verifyAlreadyMapExist() {
		crud_Operation_CommonObjects.closeAttentionPopup();
		crud_Operation_CommonObjects.clickOnClose();
	}

}
