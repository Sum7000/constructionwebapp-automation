package CRUD_Operation_Scripts;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.testng.annotations.Test;

import CRUD_Operation_POM.CRUD_Operation_CommonObjects;
import SearchEntities.Searchentitiesobjects;
import Utils.Constant;
import Utils.LoginSetup;
import mapCommonObjects.MapCommonPOM;

public class CreationOf_Category extends LoginSetup  {
	@Test
	public void creationOf_Map() throws Exception {
		MapCommonPOM mapCommonPOM=new MapCommonPOM(driver);
		CRUD_Operation_CommonObjects crud_Operation_CommonObjects=new CRUD_Operation_CommonObjects(driver);
		Searchentitiesobjects searchentitiesobjects=new Searchentitiesobjects(driver);
		
		LocalDate localDate = LocalDate.now();
		String s1 = DateTimeFormatter.ofPattern("yyy/MM/dd").format(localDate);
		
		System.out.println(this.getClass().getName()+"= Started");
		/*
		 * Create of MAP
		 */
		mapCommonPOM.ClickPlusButton_Maps();
		mapCommonPOM.verifyCreationEntityForm_isDisplay();
		crud_Operation_CommonObjects.selectEntityType_Category();
		crud_Operation_CommonObjects.selectParentEntity(Constant.parentEntity);
		crud_Operation_CommonObjects.enterEntityName(s1+Constant.creationCategory_EntityName);
		crud_Operation_CommonObjects.clickOnSave();
		mapCommonPOM.verifyAlreadyMapExist();
		searchentitiesobjects.searchAndVerify_isMapPresentInLeftTree(s1+Constant.creationCategory_EntityName);
		/*
		 * Update of MAP
		 */
		searchentitiesobjects.searchAndContextClick(s1+Constant.creationCategory_EntityName);
		mapCommonPOM.clickOnRename_ContextClick();
		crud_Operation_CommonObjects.renameEntityName(s1+Constant.UpdateCategory_EntityName);
		crud_Operation_CommonObjects.clickOnSave();
		searchentitiesobjects.searchAndVerify_isMapPresentInLeftTree(s1+Constant.UpdateCategory_EntityName);
		/*
		 * Delete of MAP
		 */
		searchentitiesobjects.searchAndContextClick(s1+Constant.UpdateCategory_EntityName);
		mapCommonPOM.clickOnDelete_ContextClick();
		crud_Operation_CommonObjects.clickOnSave();
		System.out.println(this.getClass().getName()+"= Passed");
		
	}
}
